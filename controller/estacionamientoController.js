const Estacionamiento = require("../model/Estacionamiento");

exports.createEstacionamiento = async (req, res) => {
  try {
    estacionamiento = new Estacionamiento(req.body);
    estacionamiento.creador = req.usuario.id;
    estacionamiento.save();
    res.json({ estacionamiento, msg: "Publicacion creada correctamente." });
  } catch (error) {
    res.status(500).send("Hubo un error");
  }
};

exports.obtenerMisEstacionamientos = async (req, res) => {
  try {
    const estacionamientos = await Estacionamiento.find({
      creador: req.usuario.id,
    });
    res.json({ estacionamientos });
  } catch (error) {
    res.status(500).send("Hubo un error");
  }
};

exports.editarEstacionamiento = async (req, res) => {
  try {
    let estacionamiento = req.body;
    if (!estacionamiento) {
      return res.status(404).json({ msg: "Estacionamiento no encontrado" });
    }
    if (estacionamiento.creador.toString() != req.usuario.id) {
      return res.status(401).json({ msg: "No Autorizado" });
    }
    await Estacionamiento.findByIdAndUpdate(
      { _id: estacionamiento._id },
      { $set: estacionamiento },
      { new: true }
    );
    return res.status(200).json({ msg: "Estacionamiento Actualizado" });
  } catch (error) {
    res.status(500).send("Error en el servidor");
  }
};

exports.borrarEstacionamiento = async (req, res) => {
  try {
    let estacionamiento = await Estacionamiento.findById(req.params.id);
    if (!estacionamiento) {
      return res.status(404).json({ msg: "Estacionamiento no encontrado" });
    }
    if (estacionamiento.creador.toString() !== req.usuario.id) {
      return res.status(401).json({ msg: "No Autorizado" });
    }
    await Estacionamiento.findOneAndRemove({ _id: req.params.id });
    res.json({ msg: "Proyecto eliminado " });
  } catch (error) {
    res.status(500).send("Error en el servidor");
  }
};
