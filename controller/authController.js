const Usuario = require("../model/Usuario");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.autenticarUsuario = async (req, res) => {
  const { email, password } = req.body;

  try {
    let usuario = await Usuario.findOne({ email });
    if (!usuario) {
      return res
        .status(400)
        .json({ type: "Error", msg: "El usuario no existe" });
    }

    const correctPass = await bcryptjs.compare(password, usuario.password);
    if (!correctPass) {
      return res
        .status(400)
        .json({ type: "Error", msg: "La contraseña es incorrecta" });
    }

    //si todo es correcto Creo y firmo el JWT
    const payload = {
      usuario: {
        id: usuario.id,
        tipoCuenta: usuario.tipoCuenta
      },
    };
    jwt.sign(
      payload,
      process.env.SECRETPASS,
      {
        expiresIn: 7200,
      },
      (error, token) => {
        if (error) throw error;
        res.json({ token });
      }
    );
  } catch (error) {
    console.log(error);
  }
};

//Obtiene que usuario esta autenticado
exports.usuarioAutenticado = async (req, res) => {
  try {
    const usuario = await Usuario.findById(req.usuario.id).select("-password");
    res.json({ usuario });
  } catch (error) {
    res.status(500).json({ msg: "Hubo un error" });
  }
};
