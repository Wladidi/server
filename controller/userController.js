const Usuario = require("../model/Usuario");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.createUser = async (req, res) => {

  const { rut, email, password } = req.body;
  try {
    let usuario = await Usuario.findOne({ rut });
    if (usuario) {
      return res.status(400).json({
        msg: "El rut ingresado ya se encuentra registrado.",
      });
    }
    usuario = await Usuario.findOne({ email });
    if (usuario) {
      return res.status(400).json({
        msg: "El correo ingresado ya se encuentra registrado.",
      });
    }
    //Creo el usuario
    usuario = new Usuario(req.body);

    //Encripto la contraseña

    const salt = await bcryptjs.genSalt(10);
    usuario.password = await bcryptjs.hash(password, salt);

    //Guardo el usuario
    await usuario.save();

    //Creo y firmo el JWT
    const payload = {
        usuario:{
            id: usuario.id,
            tipoCuenta: usuario.tipoCuenta
        }
    };
    jwt.sign(
      payload,
      process.env.SECRETPASS,
      {
        expiresIn: 7200,
      },
      (error, token) => {
        if (error) throw error;
        res.json({ token });
      }
    );
  } catch (error) {
    res.status(400).send("Ocurrio un error");
  }
};
