const express = require("express");
const router = express.Router();
const estacionamientoController = require("../controller/estacionamientoController");
const auth = require("../middlewares/auth");

router.post("/", auth, estacionamientoController.createEstacionamiento);

router.get("/", auth, estacionamientoController.obtenerMisEstacionamientos);

router.put("/", auth, estacionamientoController.editarEstacionamiento);

router.delete("/:id", auth, estacionamientoController.borrarEstacionamiento);

module.exports = router;
