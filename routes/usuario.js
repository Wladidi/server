// Ruta para crear usuario
const express = require("express");
const router = express.Router();
const userController = require("../controller/userController");


//Crear usuario
//api/user/create
router.post(
  "/",
  userController.createUser
);

module.exports = router;
