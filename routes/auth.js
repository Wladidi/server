// Ruta para autenticar usuario
const express = require("express");
const router = express.Router();
const authController = require("../controller/authController")
const auth = require("../middlewares/auth");

//Iniciar sesion
router.post(
  "/",
  authController.autenticarUsuario
);


router.get('/',
auth,
authController.usuarioAutenticado
)
module.exports = router;
