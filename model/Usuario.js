const mongoose = require("mongoose");

const UsuarioSchema = mongoose.Schema({
  rut: { type: String, required: true, unique: true },
  nombre: { type: String, required: true },
  apellido: { type: String, required: true },
  fechaNacimiento: { type: Date, required: true },
  password: { type: String, required: true },
  email: { type: String, required: true, unique: true, trim: true },
  comuna:{type:String,required: true},
  direccion: { type: String, required: true },
  tipoCuenta: { type: Number, required: true },
  registro: { type: Date, default: Date.now() },
});

module.exports = mongoose.model("Usuario", UsuarioSchema);
