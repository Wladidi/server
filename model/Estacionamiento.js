const mongoose = require("mongoose");

const EstacionamientoSchema = mongoose.Schema({
  comuna: {
    type: String,
    require: true,
  },
  direccion: {
    type: String,
    require: true,
  },
  horaInicio: {
    type: Date,
  },
  horaFin: { type: Date },
  telefono: {
    type: Number,
    require: true,
  },
  precio: {
    type: Number,
    require: true,
  },
  descripcion: {
    type: String,
  },
  tipoEstacionamiento: {
    type: Number,
  },
  creador: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Usuario",
  },
  fechaCreacion: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model("Estacionamiento", EstacionamientoSchema);
