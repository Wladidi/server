const express = require("express");
const conectarBD = require("./config/db");
const cors = require("cors");

//Creo el servidor (express)
const app = express();

//Habilito express.json

app.use(express.json({ extended: true }));

//Habilito cors

app.use(cors());

//Si no existe la variable de entorno
//es por que NO esta en produccion y le inserto el port 400
const PORT = process.env.PORT || 4000;

app.listen(PORT, () => {
  conectarBD();
});

//Importar rutas

app.use("/api/user/create", require("./routes/usuario"));
app.use("/api/user/auth", require("./routes/auth"));
app.use("/api/estacionamiento", require("./routes/estacionamiento"))
